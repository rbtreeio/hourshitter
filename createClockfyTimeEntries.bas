Attribute VB_Name = "Module1"
Option Explicit

Public Declare PtrSafe Function web_popen Lib "/usr/lib/libc.dylib" Alias "popen" (ByVal command As String, ByVal mode As String) As LongPtr
Public Declare PtrSafe Function web_pclose Lib "/usr/lib/libc.dylib" Alias "pclose" (ByVal file As LongPtr) As Long
Public Declare PtrSafe Function web_fread Lib "/usr/lib/libc.dylib" Alias "fread" (ByVal outStr As String, ByVal size As LongPtr, ByVal items As LongPtr, ByVal stream As LongPtr) As Long
Public Declare PtrSafe Function web_feof Lib "/usr/lib/libc.dylib" Alias "feof" (ByVal file As LongPtr) As LongPtr

Sub createClockifyTimeEntries()


Dim sht As Worksheet
Dim LastRow As Long
Dim i As Integer
Dim sQuery As String
Dim UserID As String

Sheets("Input").Select
Set sht = ActiveSheet

UserID = Range("s1").Value
LastRow = sht.Cells.Find("*", searchorder:=xlByRows, searchdirection:=xlPrevious).Row

For i = 2 To LastRow

'sQuery = "'{""start"": ""2019-11-25T12:02:07.000Z"", ""billable"": ""true"", ""description"": ""Version QA"", ""projectId"": ""5ca3ac30f15c981313d4a86f"", ""end"": ""2019-11-25T17:31:10.000Z""}'"
'Debug.Print sQuery
sQuery = "'{""" & Range("j1").Value & """: """ & Range("j" & i).Value & """, """ & Range("k1").Value & """: """ & Range("k" & i).Value & """, """ & Range("l1").Value & """: """ & Range("l" & i).Value & """, """ & Range("m1").Value & """: """ & Range("m" & i).Value & """, """ & Range("n1").Value & """: """ & Range("n" & i).Value & """}'"
Debug.Print sQuery
HTTPPost sQuery, UserID
'Debug.Print sQuery

Next


MsgBox (LastRow)
End Sub

Public Function executeInShell(web_Command As String) As String

    Dim web_File As LongPtr
    Dim web_Chunk As String
    Dim web_Read As Long

    On Error GoTo web_Cleanup

    web_File = web_popen(web_Command, "r")

    If web_File = 0 Then
        Exit Function
    End If
Debug.Print Str(web_File)
    Do While web_feof(web_File) = 0
        web_Chunk = VBA.Space$(50)
        web_Read = web_fread(web_Chunk, 1, Len(web_Chunk) - 1, web_File)
        Debug.Print Str(web_feof(web_File))
        If web_Read > 0 Then
            web_Chunk = VBA.Left$(web_Chunk, web_Read)
            executeInShell = executeInShell & web_Chunk
        End If
    Loop

web_Cleanup:

    web_pclose (web_File)

End Function



Function HTTPPost(sQuery As String, UserID As String) As String

    Dim sCmd As String
    Dim sHeader1 As String
    Dim sHeader2 As String
    Dim sUrl As String
    Dim sResult As String
    Dim lExitCode As Long

   sHeader1 = "Content-type: application/json"
    sHeader2 = "X-Api-Key: XA0GCbB5h2QwUI1L"
   
    sUrl = "https://api.clockify.me/api/v1/workspaces/5c063627b0798723e0fa22f4/user/" & UserID & "/time-entries"

    sCmd = "curl -XPOST -d " & sQuery & "" & " " & sUrl & " -H """ & sHeader1 & """" & " -H """ & sHeader2 & """"
    
    Debug.Print (sCmd)
    sResult = executeInShell(sCmd)

    ' ToDo check lExitCode

    Debug.Print (sResult)

End Function

